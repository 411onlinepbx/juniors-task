import 'bootstrap/dist/css/bootstrap.min.css'
import App from './components/App.jsx'
import { Provider } from 'react-redux'
import React from 'react'
import ReactDOM from 'react-dom'
import client from 'socket.io-client'
import createLogger from 'redux-logger'
import { getPosts } from './api/getPosts.js'
import { getUsers } from './api/getUsers.js'
import { reducer } from './reducer.js'
import { addPosts, removePost } from './reducer/posts.js'
import { applyMiddleware, createStore } from 'redux'

export const connectClient = client.connect('ws://' + DATA_HOST, { reconnect: true, timeOut: 1000 })
const store = createStore(reducer, applyMiddleware(createLogger()))
connectClient.on('connect', () => {
  console.log('connect')
  Promise.all([ getPosts(connectClient), getUsers(connectClient) ])
  .then(([ posts, users ]) => {
    const fullPosts = posts.map(post => ({
      key: post.id,
      userId: post.userId,
      name: users[post.userId],
      text: post.body,
      id: post.id
    }))
    store.dispatch(removePost())
    store.dispatch(addPosts(fullPosts))
  })
  .catch(error => {
    document.getElementById('errorblock').style.display = 'none'
    setTimeout(() => {
      document.getElementById('errorblock').style.display = ''
      console.log(error)
      document.getElementById('nameErrorblock').innerHTML = error.message
    }, 100)
  })

  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('yo')
  )
})

connectClient.on('addPost', newPost => {
  console.log('add new post')
  store.dispatch(addPosts(newPost))
})

